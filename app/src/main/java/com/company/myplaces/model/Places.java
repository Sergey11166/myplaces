package com.company.myplaces.model;

import java.util.List;

public class Places {

    private List<Place> places;

    public List<Place> getPlaces() {
        return places;
    }

    public void setPlaces(List<Place> places) {
        this.places = places;
    }
}
