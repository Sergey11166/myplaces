package com.company.myplaces.model;

import com.google.gson.annotations.SerializedName;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

public class Place {

    private long id;

    private double latitude;

    @SerializedName("longtitude")
    private double longitude;

    private String text;

    private String image;

    private String lastVisited;

    public long getId() {
        return id;
    }
    public void setId(long id) {
        this.id = id;
    }

    public double getLatitude() {
        return latitude;
    }
    public void setLatitude(double latitude) {
        this.latitude = latitude;
    }

    public double getLongitude() {
        return longitude;
    }
    public void setLongitude(double longitude) {
        this.longitude = longitude;
    }

    public String getText() {
        return text;
    }
    public void setText(String text) {
        this.text = text;
    }

    public String getImage() {
        return image;
    }
    public void setImage(String image) {
        this.image = image;
    }

    public String getLastVisited() {
        return lastVisited;
    }
    public void setLastVisited(String lastVisited) {
        this.lastVisited = lastVisited;
    }

    public Date getLastVisitedDate() {
        SimpleDateFormat sdf = new SimpleDateFormat("EEE MMM dd hh:mm:ss Z yyyy", Locale.ENGLISH);
        Date result = null;
        try {
            result = sdf.parse(lastVisited);
        } catch (ParseException ignored) {}
        return result;
    }

    public void setLastVisitedDate(Date lastVisited) {
        SimpleDateFormat sdf = new SimpleDateFormat("EEE MMM dd HH:mm:ss Z yyyy", Locale.ENGLISH);
        this.lastVisited = sdf.format(lastVisited);
    }

    @Override
    public String toString() {
        return "Place{" +
                "id=" + id +
                ", latitude=" + latitude +
                ", longitude=" + longitude +
                ", text='" + text + '\'' +
                ", image='" + image + '\'' +
                ", lastVisited=" + lastVisited +
                '}';
    }
}
