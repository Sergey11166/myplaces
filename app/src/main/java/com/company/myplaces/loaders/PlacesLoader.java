package com.company.myplaces.loaders;

import android.content.Context;
import android.support.v4.content.AsyncTaskLoader;

import com.company.myplaces.db.PlacesTable;
import com.company.myplaces.model.Place;

import java.util.List;

public class PlacesLoader extends AsyncTaskLoader<List<Place>> {

    private Context context;

    public PlacesLoader(Context context) {
        super(context);
        this.context = context;
    }

    @Override
    protected void onStartLoading() {
        super.onStartLoading();
        forceLoad();
    }

    @Override
    public List<Place> loadInBackground() {
        return PlacesTable.findAll(context);
    }
}