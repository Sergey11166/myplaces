package com.company.myplaces.db;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.provider.BaseColumns;
import android.support.annotation.NonNull;

import com.company.myplaces.model.Place;

import java.util.ArrayList;
import java.util.List;

public class PlacesTable {

    public static long save(Context context, @NonNull Place place) {
        SQLiteDatabase database = new SqliteHelper(context).getWritableDatabase();
        ContentValues values = toContentValues(place);
        long id;
        if (place.getId() != 0) {
            id = place.getId();
            String whereClause = Columns._ID + "=?";
            String[] whereArgs = {String.valueOf(place.getId())};
            database.update(Requests.TABLE_NAME, values, whereClause, whereArgs);
        } else {
            id = database.insertWithOnConflict(Requests.TABLE_NAME, null, values,
                    SQLiteDatabase.CONFLICT_REPLACE);
        }
        database.close();
        return id;
    }

    public static void saveList(Context context, @NonNull List<Place> places) {
        SQLiteDatabase database = new SqliteHelper(context).getReadableDatabase();
        for (Place place : places) {
            ContentValues values = toContentValues(place);
            database.insertWithOnConflict(Requests.TABLE_NAME, null, values,
                    SQLiteDatabase.CONFLICT_REPLACE);
        }
        database.close();
    }

    public static Place findOne(Context context, long id) {
        SQLiteDatabase database = new SqliteHelper(context).getReadableDatabase();
        String[] projection = {
                Columns._ID,
                Columns.LATITUDE,
                Columns.LONGITUDE,
                Columns.TEXT,
                Columns.IMAGE,
                Columns.LAST_VISITED};
        String selection = Columns._ID + "=?";
        String[] selectionArgs = {String.valueOf(id)};
        Cursor cursor =  database.query(Requests.TABLE_NAME, projection, selection, selectionArgs,
                null, null, null);
        Place result = listFromCursor(cursor).get(0);
        database.close();
        return result;
    }

    public static List<Place> findAll(Context context) {
        SQLiteDatabase database = new SqliteHelper(context).getReadableDatabase();
        String[] projection = {
                Columns._ID,
                Columns.LATITUDE,
                Columns.LONGITUDE,
                Columns.TEXT,
                Columns.IMAGE,
                Columns.LAST_VISITED};
        Cursor cursor = database.query(Requests.TABLE_NAME, projection, null, null, null, null, null);
        List<Place> result = listFromCursor(cursor);
        database.close();
        return result;
    }

    @NonNull
    private static List<Place> listFromCursor(@NonNull Cursor cursor) {
        List<Place> places = new ArrayList<>();
        if (!cursor.moveToFirst()) {
            return places;
        }
        try {
            do {
                places.add(fromCursor(cursor));
            } while (cursor.moveToNext());
            return places;
        } finally {
            cursor.close();
        }
    }

    @NonNull
    private static ContentValues toContentValues(@NonNull Place place) {
        ContentValues values = new ContentValues();
        values.put(Columns.LATITUDE, place.getLatitude());
        values.put(Columns.LONGITUDE, place.getLongitude());
        values.put(Columns.TEXT, place.getText());
        values.put(Columns.IMAGE, place.getImage());
        values.put(Columns.LAST_VISITED, place.getLastVisited());
        return values;
    }

    @NonNull
    private static Place fromCursor(@NonNull Cursor cursor) {
        long id = cursor.getLong(cursor.getColumnIndex(Columns._ID));
        double latitude = cursor.getDouble(cursor.getColumnIndex(Columns.LATITUDE));
        double longitude = cursor.getDouble(cursor.getColumnIndex(Columns.LONGITUDE));
        String text = cursor.getString(cursor.getColumnIndex(Columns.TEXT));
        String image = cursor.getString(cursor.getColumnIndex(Columns.IMAGE));
        String lastVisited = cursor.getString(cursor.getColumnIndex(Columns.LAST_VISITED));
        Place place = new Place();
        place.setId(id);
        place.setText(text);
        place.setLongitude(longitude);
        place.setLatitude(latitude);
        place.setImage(image);
        place.setLastVisited(lastVisited);
        return place;
    }

    public interface Requests {

        String TABLE_NAME = "PlacesTable";

        String CREATION_REQUEST = "CREATE TABLE IF NOT EXISTS " + TABLE_NAME + " (" +
                Columns._ID + " INTEGER PRIMARY KEY AUTOINCREMENT, " +
                Columns.LATITUDE + " REAL, " +
                Columns.LONGITUDE + " REAL, " +
                Columns.TEXT + " VARCHAR(200), " +
                Columns.IMAGE + " VARCHAR(200), " +
                Columns.LAST_VISITED + " VARCHAR(50)" + ");";

        String DROP_REQUEST = "DROP TABLE IF EXISTS " + TABLE_NAME;
    }

    public static abstract class Columns implements BaseColumns {
        public static final String LATITUDE = "latitude";
        public static final String LONGITUDE = "longitude";
        public static final String TEXT = "text";
        public static final String IMAGE = "image";
        public static final String LAST_VISITED = "lastVisited";
    }
}
