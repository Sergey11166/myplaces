package com.company.myplaces.api;

import com.company.myplaces.model.Places;

import retrofit.Call;
import retrofit.http.GET;

public interface PlaceService {
    @GET("files/android-middle-level-data.json")
    Call<Places> listPlaces();
}
