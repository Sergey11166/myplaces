package com.company.myplaces;

import android.app.Service;
import android.content.Intent;
import android.os.Binder;
import android.os.IBinder;

import com.company.myplaces.api.ApiFactory;
import com.company.myplaces.db.PlacesTable;
import com.company.myplaces.model.Place;
import com.company.myplaces.model.Places;

import java.util.List;

import retrofit.Callback;
import retrofit.Response;
import retrofit.Retrofit;

public class DownloadPlaceService extends Service {

    private final IBinder mBinder = new DownloadPlaceBinder();
    private DownloadCallback mDownloadCallback;

    private void startDownload() {
        ApiFactory.getPlaceService().listPlaces().enqueue(new Callback<Places>() {
            @Override
            public void onResponse(Response<Places> response, Retrofit retrofit) {
                List<Place> places = response.body().getPlaces();
                PlacesTable.saveList(DownloadPlaceService.this, places);
                mDownloadCallback.onSuccess();
            }

            @Override
            public void onFailure(Throwable t) {
                mDownloadCallback.onError(t);
            }
        });
    }

    @Override
    public IBinder onBind(Intent intent) {
        startDownload();
        return mBinder;
    }

    public void setDownloadCallback(DownloadCallback downloadCallback) {
        this.mDownloadCallback = downloadCallback;
    }

    public class DownloadPlaceBinder extends Binder {
        public DownloadPlaceService getService() {
            return DownloadPlaceService.this;
        }
    }
}
