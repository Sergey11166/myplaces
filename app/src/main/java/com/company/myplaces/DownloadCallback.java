package com.company.myplaces;

public interface DownloadCallback {

    void onSuccess();
    void onError(Throwable t);
}
