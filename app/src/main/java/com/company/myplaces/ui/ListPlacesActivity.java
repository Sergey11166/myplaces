package com.company.myplaces.ui;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.ServiceConnection;
import android.os.Build;
import android.os.Bundle;
import android.os.IBinder;
import android.support.annotation.NonNull;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.LoaderManager;
import android.support.v4.content.ContextCompat;
import android.support.v4.content.Loader;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.company.myplaces.DownloadCallback;
import com.company.myplaces.DownloadPlaceService;
import com.company.myplaces.R;
import com.company.myplaces.loaders.PlacesLoader;
import com.company.myplaces.model.Place;
import com.squareup.picasso.Picasso;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class ListPlacesActivity extends AppCompatActivity
        implements LoaderManager.LoaderCallbacks<List<Place>> {

    /**
     * Whether or not the activity is in two-pane mode, i.e. running on a tablet
     * device.
     */
    private boolean mTwoPane;

    private SimpleDateFormat mSdf;

    private SimplePlaceRecyclerViewAdapter mAdapter;

    private View mRecyclerView;
    private View mProgressView;

    private DownloadPlaceConnection mConnection;
    private boolean mBound = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_place_list);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        toolbar.setTitle(getTitle());

        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(v.getContext(), PlaceDetailActivity.class);
                startActivity(intent);
            }
        });

        mProgressView = findViewById(R.id.place_list_progress);
        mRecyclerView = findViewById(R.id.place_list);
        setupRecyclerView((RecyclerView) mRecyclerView);

        if (findViewById(R.id.place_detail_container) != null) {
            // The detail container view will be present only in the
            // large-screen layouts (res/values-w900dp).
            // If this view is present, then the
            // activity should be in two-pane mode.
            mTwoPane = true;
        }
        mSdf = new SimpleDateFormat("dd.MM.yyyy", getResources().getConfiguration().locale);
        initPlacesLoader();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
    }

    private void setupRecyclerView(@NonNull RecyclerView recyclerView) {
        mAdapter = new SimplePlaceRecyclerViewAdapter();
        recyclerView.setAdapter(mAdapter);
        recyclerView.addItemDecoration(new DividerItemDecoration(this, DividerItemDecoration.VERTICAL_LIST));
    }

    @Override
    public Loader<List<Place>> onCreateLoader(int id, Bundle args) {
        switch (id) {
            case R.id.place_loader:
                return new PlacesLoader(this);
            default:
                return null;
        }
    }

    @Override
    public void onLoadFinished(Loader<List<Place>> loader, List<Place> data) {
        Intent intent = new Intent(this, DownloadPlaceService.class);
        if (data.isEmpty()) {
            mConnection = new DownloadPlaceConnection();
            bindService(intent, mConnection, Context.BIND_AUTO_CREATE);
            showProgress(true);
        } else {
            if (mBound) stopService(intent);
            mAdapter.setData(data);
            mAdapter.notifyDataSetChanged();
            showProgress(false);
        }
    }

    @Override
    public void onLoaderReset(Loader<List<Place>> loader) {}

    private void initPlacesLoader() {
        getSupportLoaderManager().initLoader(R.id.place_loader, Bundle.EMPTY, this).forceLoad();
    }

    private void showProgress(final boolean show) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB_MR2) {
            int shortAnimTime = getResources().getInteger(android.R.integer.config_shortAnimTime);

            mRecyclerView.setVisibility(show ? View.GONE : View.VISIBLE);
            mRecyclerView.animate().setDuration(shortAnimTime).alpha(show ? 0 : 1)
                    .setListener(new AnimatorListenerAdapter() {
                        @Override
                        public void onAnimationEnd(Animator animation) {
                            mRecyclerView.setVisibility(show ? View.GONE : View.VISIBLE);
                        }
                    });

            mProgressView.setVisibility(show ? View.VISIBLE : View.GONE);
            mProgressView.animate().setDuration(shortAnimTime).alpha(show ? 1 : 0)
                    .setListener(new AnimatorListenerAdapter() {
                        @Override
                        public void onAnimationEnd(Animator animation) {
                            mProgressView.setVisibility(show ? View.VISIBLE : View.GONE);
                        }
                    });
        }
    }

    private class DownloadPlaceConnection implements ServiceConnection {

        @Override
        public void onServiceConnected(ComponentName name, final IBinder service) {
            DownloadPlaceService.DownloadPlaceBinder binder =
                    (DownloadPlaceService.DownloadPlaceBinder) service;
            final DownloadPlaceService placeService = binder.getService();
            placeService.setDownloadCallback(new DownloadCallback() {

                @Override
                public void onSuccess() {
                    initPlacesLoader();
                    if (mBound) unbindService(mConnection);
                }

                @Override
                public void onError(Throwable t) {
                    if (mBound) unbindService(mConnection);
                }
            });
            mBound = true;
        }

        @Override
        public void onServiceDisconnected(ComponentName name) {
            mBound = false;
        }
    }

    private class SimplePlaceRecyclerViewAdapter
            extends RecyclerView.Adapter<SimplePlaceRecyclerViewAdapter.ViewHolder> {

        private List<Place> mPlaces;
        private int mDiameter = (int) getResources().getDimension(R.dimen.image_item_diameter);

        public SimplePlaceRecyclerViewAdapter() {
            mPlaces = new ArrayList<>();
        }

        @Override
        public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            View view = LayoutInflater.from(parent.getContext())
                    .inflate(R.layout.place_list_content, parent, false);
            return new ViewHolder(view);
        }

        @Override
        public void onBindViewHolder(final ViewHolder holder, int position) {
            holder.mPlace = mPlaces.get(position);
            holder.mTextView.setText(mPlaces.get(position).getText());
            Date date = mPlaces.get(position).getLastVisitedDate();
            holder.mLastVisited.setText(date != null ? mSdf.format(date) : "");
            String image = mPlaces.get(position).getImage();
            if (image != null) {
                Picasso.with(holder.mView.getContext())
                        .load(image)
                        .transform(new CircleTransform())
                        .resize(mDiameter, mDiameter)
                        .into(holder.mImageView);
            } else {
                holder.mImageView.setImageDrawable(ContextCompat
                        .getDrawable(holder.mView.getContext(), R.mipmap.no_photo));
            }
            holder.mView.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            if (mTwoPane) {
                                Toast.makeText(v.getContext(), "click TwoPane " +
                                        String.valueOf(holder.mPlace.getId()), Toast.LENGTH_SHORT)
                                        .show();
                            } else {
                                Context context = v.getContext();
                                Intent intent = new Intent(context, PlaceDetailActivity.class);
                                intent.putExtra(PlaceDetailFragment.ARG_PLACE_ID, holder.mPlace.getId());
                                context.startActivity(intent);
                            }
                        }
                    });
        }

        @Override
        public int getItemCount() {
            return mPlaces.size();
        }

        public void setData(List<Place> places) {
            mPlaces = places;
        }

        public class ViewHolder extends RecyclerView.ViewHolder {
            public final View mView;
            public final ImageView mImageView;
            public final TextView mTextView;
            public final TextView mLastVisited;
            public Place mPlace;

            public ViewHolder(View view) {
                super(view);
                mView = view;
                mImageView = (ImageView) view.findViewById(R.id.image);
                mTextView = (TextView) view.findViewById(R.id.text);
                mLastVisited = (TextView) view.findViewById(R.id.last_visited);
            }

            @Override
            public String toString() {
                return super.toString() + " '" + mTextView.getText() + "'";
            }
        }
    }
}
