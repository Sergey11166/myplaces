package com.company.myplaces.ui;

import android.Manifest;
import android.annotation.SuppressLint;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.LoaderManager;
import android.support.v4.content.Loader;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.company.myplaces.R;
import com.company.myplaces.loaders.PlacesLoader;
import com.company.myplaces.model.Place;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.LatLngBounds;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class MapsActivity extends AppCompatActivity implements
        OnMapReadyCallback,
        GoogleMap.OnInfoWindowClickListener,
        LoaderManager.LoaderCallbacks<List<Place>> {

    public static final String ARG_PLACE_ID = "item_id";

    private GoogleMap mMap;
    private long mPlaceId;
    private List<Place> mPlaces;
    private Map<String, Long> mPlacesMap;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_maps);

        mPlaceId = getIntent().getLongExtra(ARG_PLACE_ID, -1);

        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(view.getContext(), PlaceDetailActivity.class);
                startActivity(intent);
            }
        });

        // Obtain the SupportMapFragment and get notified when the map is ready to be used.
        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);
    }

    private void addMarkersToMap() {
        mMap.clear();
        if (mPlaces.size() == 0) {
            Toast.makeText(MapsActivity.this, "Places not found", Toast.LENGTH_SHORT).show();
            return;
        }
        LatLngBounds.Builder bounds = new LatLngBounds.Builder();
        if (mPlacesMap == null) mPlacesMap = new HashMap<>();
        else mPlacesMap.clear();
        for (Place p : mPlaces) {
            LatLng latLng = new LatLng(p.getLatitude(), p.getLongitude());
            Marker marker = mMap.addMarker(new MarkerOptions()
                    .title(p.getText())
                    .snippet(latLng.longitude + " : " + latLng.latitude)
                    .position(latLng));
            bounds.include(latLng);
            mPlacesMap.put(marker.getTitle(), p.getId());
        }
        Place selectedPlace = null;
        for (Place place : mPlaces) {
            if (mPlaceId == place.getId()){
                selectedPlace = place;
                break;
            }
        }
        if (selectedPlace != null) {
            mMap.animateCamera(CameraUpdateFactory.newLatLngZoom(new LatLng(
                    selectedPlace.getLatitude(), selectedPlace.getLongitude()), 13));
        }
    }


    /**
     * Manipulates the map once available.
     * This callback is triggered when the map is ready to be used.
     * This is where we can add markers or lines, add listeners or move the camera. In this case,
     * we just add a marker near Sydney, Australia.
     * If Google Play services is not installed on the device, the user will be prompted to install
     * it inside the SupportMapFragment. This method will only be triggered once the user has
     * installed Google Play services and returned to the app.
     */
    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;
        mMap.setInfoWindowAdapter(new CustomInfoWindowAdapter());
        mMap.setOnInfoWindowClickListener(this);
        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION)
                != PackageManager.PERMISSION_GRANTED
                && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION)
                != PackageManager.PERMISSION_GRANTED) {
            return;
        }
        mMap.setMyLocationEnabled(true);
        mMap.getUiSettings().setMapToolbarEnabled(false);
        if (mPlaces != null) {
            addMarkersToMap();
        } else {
            initPlacesLoader();
        }
    }

    @Override
    public void onInfoWindowClick(Marker marker) {
        long placeId = mPlacesMap.get(marker.getTitle());
        Intent intent = new Intent(this, PlaceDetailActivity.class);
        intent.putExtra(PlaceDetailFragment.ARG_PLACE_ID, placeId);
        startActivity(intent);
    }

    @Override
    public Loader<List<Place>> onCreateLoader(int id, Bundle args) {
        switch (id) {
            case R.id.place_loader:
                return new PlacesLoader(this);
            default:
                return null;
        }
    }

    @Override
    public void onLoadFinished(Loader<List<Place>> loader, List<Place> data) {
        mPlaces = data;
        if (mMap != null) {
            addMarkersToMap();
        }
    }

    @Override
    public void onLoaderReset(Loader<List<Place>> loader) {}

    private void initPlacesLoader() {
        getSupportLoaderManager().initLoader(R.id.place_loader, Bundle.EMPTY, this).forceLoad();
    }

    private class CustomInfoWindowAdapter implements GoogleMap.InfoWindowAdapter {

        private final View mContents;

        @SuppressLint("InflateParams")
        CustomInfoWindowAdapter() {
            mContents = getLayoutInflater().inflate(R.layout.custom_info_contents, null);
        }

        @Override
        public View getInfoWindow(Marker marker) {
            return null;
        }

        @Override
        public View getInfoContents(Marker marker) {

            ((ImageView) mContents.findViewById(R.id.image)).setImageResource(R.mipmap.vector);

            String title = marker.getTitle();
            TextView titleUi = (TextView) mContents.findViewById(R.id.title);
            if (title != null) {
                titleUi.setText(title);
            } else {
                titleUi.setText("");
            }

            String snippet = marker.getSnippet();
            TextView snippetUi = (TextView) mContents.findViewById(R.id.snippet);
            if (snippet != null) {
                snippetUi.setText(snippet);
            } else {
                snippetUi.setText("");
            }

            return mContents;
        }
    }
}
