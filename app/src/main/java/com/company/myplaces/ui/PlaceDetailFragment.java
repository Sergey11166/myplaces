package com.company.myplaces.ui;

import android.Manifest;
import android.app.Activity;
import android.app.DatePickerDialog;
import android.app.Dialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Point;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.annotation.NonNull;
import android.support.design.widget.CollapsingToolbarLayout;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.Fragment;
import android.support.v4.app.NavUtils;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.view.ActionMode;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Toast;

import com.company.myplaces.ContentUtils;
import com.company.myplaces.R;
import com.company.myplaces.db.PlacesTable;
import com.company.myplaces.model.Place;
import com.squareup.picasso.Picasso;

import java.io.File;
import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

public class PlaceDetailFragment extends Fragment implements ActionMode.Callback {

    public static final String ARG_PLACE_ID = "item_id";

    private final static int PERMISSIONS_REQUEST_CODE_LOCATION = 0;
    private final static int PERMISSIONS_REQUEST_CODE_STORAGE_TAKE_PHOTO = 1;
    private final static int PERMISSIONS_REQUEST_CODE_STORAGE_CHOICE_PHOTO = 2;

    private static final int REQUEST_PICK_IMAGE = 0;
    private static final int REQUEST_TAKE_PHOTO = 1;

    private Place mPlace;
    private ActionMode mActionMode;
    private boolean mCancel;
    private String mTempImage;
    private String mCurrentPhotoPath;

    private EditText mText;
    private EditText mLongitude;
    private EditText mLatitude;
    private ImageView mImageView;
    private Button mDateButton;

    private static EditText lastVisited;
    private static SimpleDateFormat sdf;

    public PlaceDetailFragment() {
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        sdf = new SimpleDateFormat("dd.MM.yyyy", getContext().getResources().getConfiguration().locale);

        if (getArguments().containsKey(ARG_PLACE_ID)) {
            long id = getArguments().getLong(ARG_PLACE_ID);
            if (id > 0) {
                mPlace = PlacesTable.findOne(getContext(), id);
            }
        }
        setHasOptionsMenu(true);
        Activity activity = getActivity();
        CollapsingToolbarLayout collapsingToolbarLayout = (CollapsingToolbarLayout) activity
                .findViewById(R.id.toolbar_layout);
        if (collapsingToolbarLayout != null) {
            collapsingToolbarLayout.setTitle(mPlace != null ? mPlace.getText() :
                    getString(R.string.new_place));
            collapsingToolbarLayout.setExpandedTitleColor(
                    ContextCompat.getColor(getContext(), android.R.color.transparent));
        }

        mImageView = (ImageView) activity.findViewById(R.id.place_detail_image);
        if (mPlace != null) {
            mTempImage = mPlace.getImage();
        } else {
            mTempImage = null;
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.place_detail, container, false);

        mText = (EditText) rootView.findViewById(R.id.text);
        mLongitude = (EditText) rootView.findViewById(R.id.longitude);
        mLatitude = (EditText) rootView.findViewById(R.id.latitude);
        lastVisited = (EditText) rootView.findViewById(R.id.last_visited);
        mDateButton = (Button) rootView.findViewById(R.id.date_button);
        mDateButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onDateButtonClick();
            }
        });

        updateUI();

        return rootView;
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        inflater.inflate(R.menu.menu_place_detail_main, menu);
        super.onCreateOptionsMenu(menu, inflater);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                NavUtils.navigateUpTo(getActivity(), new Intent(getContext(), ListPlacesActivity.class));
                return true;
            case R.id.edit:
                startActionMode();
                return true;
            case R.id.map:
                checkLocationPermissions();
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == REQUEST_PICK_IMAGE && resultCode == Activity.RESULT_OK
                && data != null && data.getData() != null) {
            Uri contentUri = data.getData();
            mCurrentPhotoPath = ContentUtils.getPath(getContext(), contentUri);
            if (mCurrentPhotoPath != null) {
                Uri uri = Uri.fromFile(new File(mCurrentPhotoPath));
                String path = uri.toString();
                loadIntoImageView(path);
                mTempImage = path;
            }
        } else if (requestCode == REQUEST_TAKE_PHOTO && resultCode == Activity.RESULT_OK) {
            if (mCurrentPhotoPath != null) {
                galleryAddPic();
                Uri uri = Uri.fromFile(new File(mCurrentPhotoPath));
                String path = uri.toString();
                loadIntoImageView(path);
                mTempImage = path;
                mCurrentPhotoPath = null;
            }
        }
    }

    private void startActionMode() {
        AppCompatActivity activity = (AppCompatActivity) getActivity();
        mActionMode = activity.startSupportActionMode(this);
    }

    @Override
    public boolean onCreateActionMode(ActionMode mode, Menu menu) {
        mode.getMenuInflater().inflate(R.menu.menu_place_detail_action_mode, menu);
        mCancel = true;
        enableViews(true);
        return true;
    }

    @Override
    public boolean onPrepareActionMode(ActionMode mode, Menu menu) {
        return false;
    }

    @Override
    public boolean onActionItemClicked(ActionMode mode, MenuItem item) {
        switch (item.getItemId()) {
            case R.id.photo:
                showChoicePhotoActionDialog();
                return true;
            case R.id.done:
                if (validateData()){
                    savePlace();
                    mActionMode.finish();
                    enableViews(false);
                    mCancel = false;
                }
                return true;
            case R.id.cancel:
                mActionMode.finish();
                return true;
            default: return false;
        }
    }

    @Override
    public void onDestroyActionMode(ActionMode mode) {
        if (mode.isUiFocusable() && mPlace == null) getActivity().finish();
        if (mCancel) updateUI();
        enableViews(false);
    }

    private void updateUI() {
        CollapsingToolbarLayout collapsingToolbarLayout = (CollapsingToolbarLayout) getActivity()
                .findViewById(R.id.toolbar_layout);
        if (mPlace != null) {
            enableViews(false);
            if (collapsingToolbarLayout != null) {
                collapsingToolbarLayout.setTitle(mPlace.getText());
            }
            mText.setText(mPlace.getText());
            mLongitude.setText(String.valueOf(mPlace.getLongitude()));
            mLatitude.setText(String.valueOf(mPlace.getLatitude()));
            Date date = mPlace.getLastVisitedDate();
            lastVisited.setText(date != null ? sdf.format(date) : "");
            String path = mPlace.getImage();
            if (path != null) {
                loadIntoImageView(path);
            } else {
                mImageView.setImageDrawable(ContextCompat.getDrawable(getContext(), R.mipmap.no_photo));
            }
        } else {
            startActionMode();
            mImageView.setImageDrawable(ContextCompat.getDrawable(getContext(), R.mipmap.no_photo));
            lastVisited.setText(sdf.format(new Date()));
            if (collapsingToolbarLayout != null) {
                collapsingToolbarLayout.setTitle(getString(R.string.new_place));
            }
        }
    }

    private void loadIntoImageView(String path) {

        Point size = new Point();
        getActivity().getWindowManager().getDefaultDisplay().getSize(size);

        //noinspection SuspiciousNameCombination
        Picasso.with(getContext())
                .load(path)
                .resize(size.x, size.x)
                .into(mImageView);
    }

    private void checkLocationPermissions() {
        if (ActivityCompat.checkSelfPermission(getActivity(), Manifest.permission.ACCESS_FINE_LOCATION)
                != PackageManager.PERMISSION_GRANTED) {
            requestPermissions(new String[]{Manifest.permission.ACCESS_FINE_LOCATION},
                    PERMISSIONS_REQUEST_CODE_LOCATION);
        }else {
            onLocationPermissionsGranted();
        }
    }

    private void checkStoragePermission(int code) {
        if (ActivityCompat.checkSelfPermission(getActivity(), Manifest.permission.WRITE_EXTERNAL_STORAGE)
                != PackageManager.PERMISSION_GRANTED){
            requestPermissions(new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE}, code);
        }else {
            onStoragePermissionsGranted(code);
        }
    }

    private void onLocationPermissionsGranted() {
        Intent intent = new Intent(getContext(), MapsActivity.class);
        intent.putExtra(MapsActivity.ARG_PLACE_ID, mPlace.getId());
        startActivity(intent);
    }

    private void onStoragePermissionsGranted(int code) {
        switch (code) {
            case PERMISSIONS_REQUEST_CODE_STORAGE_TAKE_PHOTO:
                dispatchTakePictureIntent();
                return;
            case PERMISSIONS_REQUEST_CODE_STORAGE_CHOICE_PHOTO:
                Intent intent = new Intent();
                intent.setType("image/*");
                intent.setAction(Intent.ACTION_GET_CONTENT);
                startActivityForResult(Intent.createChooser(intent, "Select Picture"),
                        REQUEST_PICK_IMAGE);
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String permissions[],
                                           @NonNull int[] grantResults) {
        switch (requestCode) {
            case PERMISSIONS_REQUEST_CODE_LOCATION:
                if (grantResults.length == 1 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    onLocationPermissionsGranted();
                }
                return;
            case PERMISSIONS_REQUEST_CODE_STORAGE_TAKE_PHOTO:
                if (grantResults.length == 1 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    onStoragePermissionsGranted(PERMISSIONS_REQUEST_CODE_STORAGE_TAKE_PHOTO);
                }
                return;
            case PERMISSIONS_REQUEST_CODE_STORAGE_CHOICE_PHOTO:
                if (grantResults.length == 1 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    onStoragePermissionsGranted(PERMISSIONS_REQUEST_CODE_STORAGE_CHOICE_PHOTO);
                }
                break;
        }
    }

    private void onDateButtonClick() {
        DialogFragment newFragment = new DatePickerFragment();
        newFragment.show(getActivity().getSupportFragmentManager(), "datePicker");
    }

    private void savePlace() {
        String text = mText.getText().toString();
        double longitude = Double.parseDouble(mLongitude.getText().toString());
        double latitude = Double.parseDouble(mLatitude.getText().toString());
        if (mPlace == null) mPlace = new Place();
        mPlace.setText(text);
        mPlace.setLongitude(longitude);
        mPlace.setLatitude(latitude);
        mPlace.setImage(mTempImage);
        try {
            mPlace.setLastVisitedDate(sdf.parse(lastVisited.getText().toString()));
        } catch (ParseException ignored) {}
        long id = PlacesTable.save(getContext(), mPlace);
        mPlace.setId(id);
    }

    private boolean validateData() {

        mText.setError(null);
        String text = mText.getText().toString();
        String lat = mLatitude.getText().toString();
        String lon = mLongitude.getText().toString();

        boolean cancel = false;
        View focusView = null;

        // check for a valid text
        if (text.isEmpty()) {
            Toast.makeText(getContext(),
                    getString(R.string.validate_error_address_not_empty), Toast.LENGTH_LONG).show();
            focusView = mText;
            cancel = true;
        } else if (text.length() > 50) {
            Toast.makeText(getContext(),
                    getString(R.string.validate_error_length_address_more_50), Toast.LENGTH_LONG).show();
            focusView = mText;
            cancel = true;
        }

        // check for a valid latitude
        if (!cancel) {
            if (lat.isEmpty()) {
                Toast.makeText(getContext(),
                        getString(R.string.validate_error_coordinates_not_empty), Toast.LENGTH_LONG).show();
                focusView = mLatitude;
                cancel = true;
            } else if (!lat.matches("^-?([1-8]?[1-9]|[1-9]0)\\.{1}\\d{1,6}$")) {
                Toast.makeText(getContext(),
                        getString(R.string.validate_error_invalid_characters_coordinates), Toast.LENGTH_LONG).show();
                focusView = mLatitude;
                cancel = true;
            }
        }

        // check for a valid longitude
        if (!cancel) {
            if (lon.isEmpty()) {
                Toast.makeText(getContext(),
                        getString(R.string.validate_error_coordinates_not_empty), Toast.LENGTH_LONG).show();
                focusView = mLongitude;
                cancel = true;
            } else if (!lon.matches("^-?([1]?[1-7][1-9]|[1]?[1-8][0]|[1-9]?[0-9])\\.{1}\\d{1,6}$")) {
                Toast.makeText(getContext(),
                        getString(R.string.validate_error_invalid_characters_coordinates), Toast.LENGTH_LONG).show();
                focusView = mLongitude;
                cancel = true;
            }
        }

        if (cancel) {
            focusView.requestFocus();
            return false;
        } else {
            return true;
        }


    }

    private void enableViews(boolean enabled) {
        mText.setEnabled(enabled);
        mLatitude.setEnabled(enabled);
        mLongitude.setEnabled(enabled);
        mDateButton.setVisibility(enabled ? View.VISIBLE : View.GONE);

        mText.setFocusableInTouchMode(enabled);
        mText.setFocusable(enabled);
        mLatitude.setFocusableInTouchMode(enabled);
        mLatitude.setFocusable(enabled);
        mLongitude.setFocusableInTouchMode(enabled);
        mLongitude.setFocusable(enabled);
    }

    private void showChoicePhotoActionDialog() {
        AlertDialog.Builder builder = new AlertDialog.Builder(getContext());
        final ArrayAdapter<String> arrayAdapter = new ArrayAdapter<>(getContext(),
                R.layout.item_dialog_siglechoice);
        arrayAdapter.add(getString(R.string.dialog_item_choice_from_gallery));
        arrayAdapter.add(getString(R.string.dialog_item_take_photo));
        arrayAdapter.add(getString(R.string.dialog_item_delete));
        builder.setAdapter(arrayAdapter, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                switch (which) {
                    case 0: //choose from gallery
                        checkStoragePermission(PERMISSIONS_REQUEST_CODE_STORAGE_CHOICE_PHOTO);
                        break;
                    case 1: //take photo
                        checkStoragePermission(PERMISSIONS_REQUEST_CODE_STORAGE_TAKE_PHOTO);
                        break;
                    case 2: //delete photo
                        mTempImage = null;
                        mImageView.setImageDrawable(ContextCompat
                                .getDrawable(getContext(), R.mipmap.no_photo));
                        break;
                    default:
                        dialog.dismiss();
                }
            }
        });
        AlertDialog alert = builder.create();
        alert.setCanceledOnTouchOutside(true);
        alert.show();
    }

    private void dispatchTakePictureIntent() {
        Intent takePictureIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        File f;
        try {
            f = createImageFile();
            mCurrentPhotoPath = f.getAbsolutePath();
            takePictureIntent.putExtra(MediaStore.EXTRA_OUTPUT, Uri.fromFile(f));
        } catch (IOException e) {
            e.printStackTrace();
            mCurrentPhotoPath = null;
        }
        startActivityForResult(takePictureIntent, REQUEST_TAKE_PHOTO);
    }

    private File createImageFile() throws IOException {
        String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss",
                getContext().getResources().getConfiguration().locale).format(new Date());
        String imageFileName = "IMAGE_" + timeStamp + "_";
        File albumF = getAlbumDir();
        return File.createTempFile(imageFileName, ".jpg", albumF);
    }

    private File getAlbumDir() {
        File storageDir = null;
        if (Environment.MEDIA_MOUNTED.equals(Environment.getExternalStorageState())) {
            storageDir = new File(Environment.getExternalStorageDirectory() +
                    "/dcim/" + getString(R.string.app_name));
            if (!storageDir.mkdirs()) {
                if (! storageDir.exists()){
                    return null;
                }
            }
        }
        return storageDir;
    }

    private void galleryAddPic() {
        Intent mediaScanIntent = new Intent(Intent.ACTION_MEDIA_SCANNER_SCAN_FILE);
        File f = new File(mCurrentPhotoPath);
        Uri contentUri = Uri.fromFile(f);
        mediaScanIntent.setData(contentUri);
        getActivity().sendBroadcast(mediaScanIntent);
    }

    public static class DatePickerFragment extends DialogFragment
            implements DatePickerDialog.OnDateSetListener {

        @NonNull
        @Override
        public Dialog onCreateDialog(Bundle savedInstanceState) {
            final Calendar c = Calendar.getInstance();
            Date date = null;
            try {
                date = sdf.parse(lastVisited.getText().toString());
            } catch (ParseException ignored) {}
            if (date != null) c.setTime(date);
            int year = c.get(Calendar.YEAR);
            int month = c.get(Calendar.MONTH);
            int day = c.get(Calendar.DAY_OF_MONTH);
            return new DatePickerDialog(getActivity(), this, year, month, day);
        }

        public void onDateSet(DatePicker view, int year, int month, int day) {
            Calendar calendar = Calendar.getInstance();
            calendar.set(Calendar.YEAR, year);
            calendar.set(Calendar.MONTH, month);
            calendar.set(Calendar.DAY_OF_MONTH, day);
            lastVisited.setText(sdf.format(calendar.getTime()));
        }
    }
}
